package org.processmining.dialogs;

/**
 * Created by Ivan on 10.03.2016.
 */
public interface Verifiable
{
    boolean verify();
}
