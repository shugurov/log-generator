import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;

/**
 * @author Ivan Shugurov
 * Created on 03.09.2014.
 */

public class TestTimestamps
{

    @Plugin(name = "Test timestamps", returnTypes = {String.class}, returnLabels = {""}, parameterLabels = "Log")
    @UITopiaVariant(affiliation = "", email = "", author = "")
    public String test(PluginContext context, XLog log)
    {
        int traceNumber = 1;
        boolean errorsOccur = false;
        for (XTrace trace : log)
        {
            long timestamp = 0;
            for (XEvent event : trace)
            {
                Object objTime = event.getAttributes().get("time:timestamp");
                long time = ((XAttributeTimestamp)objTime).getValueMillis();
                String eventName = event.getAttributes().get("concept:name").toString();
                if (time < timestamp)
                {
                    errorsOccur = true;
                    System.out.println("Error in trace " + traceNumber + "; Event name: " + eventName + "; Timestamp: " + time);
                }
                timestamp = time;
            }
            traceNumber++;
        }
        if (errorsOccur)
        {
            return "Errors";
        }
        else
        {
            return "Correct";
        }
    }
}
