import sys
from random import uniform
from random import choice

# type, name, arguments = sys.argv[1], sys.argv[2], sys.argv[3::]
type, arguments = sys.argv[1], sys.argv[2::]

if type == 'activity' or type == 'subprocess':
    input_data_objects = {}
    output_data_objects = []

    for i in xrange(len(arguments)):
        if arguments[i] == '-':
            break
        else:
            key, value = arguments[i].split(':')
            input_data_objects[key] = value

    value = float(input_data_objects['B'])
    output_data_objects = [(arguments[j], value * 2) for j in xrange(i + 1, len(arguments))]

    output_array = [object for object in output_data_objects]
    output = ''

    for key, value in output_data_objects:
       arg = '"' + str(key) + ":" + str(value) + '" '
       output += arg

    print output


