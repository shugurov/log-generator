import org.deckfour.xes.model.XAttributeTimestamp;
import org.deckfour.xes.model.XEvent;
import org.deckfour.xes.model.XLog;
import org.deckfour.xes.model.XTrace;
import org.processmining.contexts.uitopia.annotations.UITopiaVariant;
import org.processmining.framework.plugin.PluginContext;
import org.processmining.framework.plugin.annotations.Plugin;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Ivan Shugurov on 20.09.2014.
 */
public class TestTimestampsAndResources
{
    @Plugin(name = "Test timestamps and resources", returnLabels = {"Correctness"}, returnTypes = {String.class}, parameterLabels = {"Event log"})
    @UITopiaVariant(affiliation = "hse", email = "", author = "")
    public String test(PluginContext context, XLog log)
    {
        int traceNumber = 1;
        boolean errorsOccur = false;
        for (XTrace trace : log)
        {
            Map<String, Boolean> resourcesState = new HashMap<String, Boolean>(); //resource name -> isIdle
            long timestamp = 0;
            for (XEvent event : trace)
            {
                Object objTime = event.getAttributes().get("time:timestamp");
                long time = ((XAttributeTimestamp) objTime).getValueMillis();
                String eventName = event.getAttributes().get("concept:name").toString();
                String lifecycle = event.getAttributes().get("lifecycle:transition").toString();
                boolean isStartEvent = lifecycle.equals("start");
                String resourceName = event.getAttributes().get("org:resource").toString();
                if (isStartEvent)
                {
                    if (resourcesState.containsKey(resourceName))
                    {
                        boolean isIdle = resourcesState.get(resourceName);
                        if (isIdle)
                        {
                            resourcesState.put(resourceName, false);
                        }
                        else
                        {
                            errorsOccur = true;
                            System.out.println("Error with resource in trace " + traceNumber + "; Event name: " + eventName + "; Attempt to use not idle resource (1)");
                        }
                    }
                    else
                    {
                        resourcesState.put(resourceName, false);
                    }
                }
                else
                {
                    boolean isIdle = resourcesState.get(resourceName);
                    if (isIdle)
                    {
                        errorsOccur = true;
                        System.out.println("Error with resource (it's idle just before releasing) in trace " + traceNumber + "; Event name:  + eventName (2)");
                    }
                    else
                    {
                        resourcesState.put(resourceName, true);
                    }
                }
                if (time < timestamp)
                {
                    errorsOccur = true;
                    System.out.println("Error in trace " + traceNumber + "; Event name: " + eventName + "; Timestamp: " + time);
                }
                timestamp = time;
            }
            traceNumber++;
        }
        if (errorsOccur)
        {
            return "Errors";
        }
        else
        {
            return "Correct";
        }
    }
}
